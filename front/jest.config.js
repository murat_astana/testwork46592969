module.exports = {
  preset: '@vue/cli-plugin-unit-jest',
  //Будем делать покрытие тестов
  collectCoverage: true,

  //90% покрытие теста
  coverageThreshold: {
    global: {
      statements: 80
    }
  },
  transform: {
    '^.+\\.vue$': 'vue-jest',
    '^.+\\.js$': 'babel-jest'
  }
}
