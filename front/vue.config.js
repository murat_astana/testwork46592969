module.exports = {
  devServer: {
    disableHostCheck: true,
    port: 8080
  },
  runtimeCompiler: true

}
