import Vue from 'vue'
import VueRouter from 'vue-router'

import { guard } from '@/router/guard'
import store from '@/store/index'

import Login from '@/views/Login'
import PasswordNoteList from '@/views/PasswordNote/Main'
import PasswordNoteAdd from '@/views/PasswordNote/Add'
import PasswordNoteEdit from '@/views/PasswordNote/Edit'
import PasswordNoteShow from '@/views/PasswordNote/Show'

Vue.use(VueRouter)

const routes = [
  {
    path: '/password-notes',
    name: 'PasswordNoteList',
    component: PasswordNoteList
  },
  {
    path: '/password-notes/add',
    name: 'PasswordNoteAdd',
    component: PasswordNoteAdd
  },
  {
    path: '/password-notes/edit/:id',
    name: 'PasswordNoteEdit',
    component: PasswordNoteEdit,
    props: true
  },
  {
    path: '/password-notes/show/:id',
    name: 'PasswordNoteShow',
    component: PasswordNoteShow,
    props: true
  },
  {
    path: '/login',
    name: 'login',
    component: Login,
    meta: {
      layout: 'auth-layout'
    }
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  guard({ to, from, next, store })
})

export default router
