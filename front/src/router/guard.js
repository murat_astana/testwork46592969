import { auth } from '@/router/middleware/auth'
import { other } from '@/router/middleware/other'

export function guard({ to, next, store }) {
  if (!store.state.Auth.isLoggedIn) {
    nextByGuard({
      to,
      next,
      pages: other
    })
    return
  }

  nextByGuard({
    to,
    next,
    pages: auth
  })
}

function nextByGuard({ to, next, pages }) {
  if (pages.includes(to.name)) {
    return next()
  } else {
    return next({ name: pages[0] })
  }
}
