
export const columns = [
  {
    title: '№',
    dataIndex: 'id',
    key: 'id',
    width: '10%'
  },
  {
    title: 'Наименование',
    dataIndex: 'name',
    key: 'name'
  },
  {
    title: 'Ссылка',
    dataIndex: 'url',
    key: 'url'
  },
  {
    title: 'Логин',
    dataIndex: 'login',
    key: 'login'
  },
  {
    title: 'Пароль',
    dataIndex: 'password',
    key: 'password'
  },
  {
    title: 'Заметка',
    dataIndex: 'note',
    key: 'note'
  },
  {
    key: 'action',
    scopedSlots: { customRender: 'action' },
    fixed: 'right',
    width: 100
  }
]
