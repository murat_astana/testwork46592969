export function ObjectToArray(data) {
  let _new = Object.entries(data).map(type => {
    return {
      id: +type[0],
      value: type[1]
    }
  })

  return _new
}
