const getItem = key => {
  try {
    return JSON.parse(window.localStorage.getItem(key))
  } catch (e) {
    console.log('Error getting data from localStore: ', e)
    return null
  }
}

const setItem = (key, data) => {
  try {
    window.localStorage.setItem(key, JSON.stringify(data))
  } catch (e) {
    console.log('Error saving data from localStore: ', e)
  }
}

export { getItem, setItem }
