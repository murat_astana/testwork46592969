import { getAxios } from '@/api/request'

export function loadAllData(api) {
  return new Promise(resolve => {
    let _page = 1
    let _allPage = 1
    let _all = []

    for (let i = 1; i <= _allPage; i++) {
      getAxios(`${api}?page=${_page}`).then(responsive => {
        _allPage = responsive.data.meta.last_page || 1
        _all.push(...responsive.data.data)
      })
    }

    resolve(_all)
  })
}
