import { stringify, parse } from 'query-string'

export function queryString(url) {
  const parsedUrl = parse(url)
  const stringifyedParams = stringify({
    limit: 10,
    offset: this.offset,
    ...parsedUrl.query
  })

  return `${parsedUrl.url}?${stringifyedParams}`
}
