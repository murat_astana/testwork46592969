export async function handlerErrors({ message, errors }) {
  let _error = ''

  if (!Object.keys(errors).length) return message

  Object.keys(errors).map(e => {
    _error += errors[e][0]
  })

  return _error
}
