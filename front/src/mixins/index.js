import { admin } from '@/router/middleware/admin'
import { director } from '@/router/middleware/director'
import { manager_active } from '@/router/middleware/manager_active'
import { manager_du } from '@/router/middleware/manager_du'
import { manager_rent } from '@/router/middleware/manager_rent'
import { manager_confiscat } from '@/router/middleware/manager_confiscat'
import { manager_other } from '@/router/middleware/manager_other'
import { worker_active } from '@/router/middleware/worker_active'
import { worker_du } from '@/router/middleware/worker_du'
import { worker_rent } from '@/router/middleware/worker_rent'
import { worker_confiscat } from '@/router/middleware/worker_confiscat'
import { worker_other } from '@/router/middleware/worker_other'
import { mapGetters } from 'vuex'

export const defaultMixin = {
  computed: {
    ...mapGetters({
      currentUser: 'Auth/user'
    }),
    typeUser() {
      switch (this.currentUser.type_id) {
        case 1:
          return admin
        case 2:
          return director
        case 3:
          switch (this.currentUser.org_structure_id) {
            case 1:
              return manager_active
            case 2:
              return manager_du
            case 3:
              return manager_rent
            case 4:
              return manager_confiscat
            default:
              return manager_other
          }
        case 4:
          switch (this.currentUser.org_structure_id) {
            case 1:
              return worker_active
            case 2:
              return worker_du
            case 3:
              return worker_rent
            case 4:
              return worker_confiscat
            default:
              return worker_other
          }
      }
      return []
    }
  },
  methods: {
    filterOption(input, option) {
      return (
        option.componentOptions.children[0].text
          .toUpperCase()
          .indexOf(input.toUpperCase()) >= 0
      )
    }
  }
}
