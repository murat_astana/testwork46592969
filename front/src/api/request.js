import axios from '@/api/axios'

function objectToUrlParam(obj) {
  let str = ''
  for (var key in obj) {
    if (typeof obj[key] === 'object') {
      for (var sub_key in obj[key]) {
        if (str != '') {
          str += '&'
        }
        str +=
          key +
          '[' +
          sub_key +
          ']' +
          '=' +
          encodeURIComponent(obj[key][sub_key])
      }
    } else {
      if (str != '') {
        str += '&'
      }

      str += key + '=' + encodeURIComponent(obj[key])
    }
  }

  return str
}

const getAxios = (api, params = {}) =>
  axios.get(api + '?' + objectToUrlParam(params))
const postAxios = (api, data, option = {}) => axios.post(api, data, option)
const putAxios = (api, data) => axios.put(api, data)
const deleteAxios = (api, data = {}) => axios.delete(api, data)

export { getAxios, postAxios, putAxios, deleteAxios }
