import axios from 'axios'
import store from '@/store'
import router from '@/router'
import { handlerErrors } from '@/helpers/handlerErrors'

axios.defaults.baseURL = process.env.VUE_APP_SERVER_URL

const configInterceptor = config => {
  config.headers.Authorization = `Bearer ${localStorage.getItem('accessToken')}`
  return config
}

const errorInterceptor = async error => {
  if (!error.response) {
    console.warn('Network/Server error')
    return Promise.reject(error)
  }

  switch (error.response.status) {
    case 400:
      console.error(error.response.status, error.message)
      break

    case 401:
      await store.dispatch('Auth/logout')
      await router.push({ name: 'login' })

      break

    case 422:
    case 500:
      break

    default:
      error.response.data.message = await handlerErrors(error.response.data)
      console.error(error.response.status, error.message)
  }

  return Promise.reject(error)
}

const responseInterceptor = async response => {
  switch (response.status) {
    case 200:
      // yay!
      break
    // any other cases
    default:
    // default case
  }

  return response
}

axios.interceptors.request.use(configInterceptor)
axios.interceptors.response.use(responseInterceptor, errorInterceptor)

export default axios
