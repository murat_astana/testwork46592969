import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

// GlobalStyle
import '@/style/index.scss'

// AntDesignUI
import './plugins/ant_ui'

// GlobalComponents
import '@/components/global'

Vue.config.productionTip = false

new Vue({
  store,
  router,
  render: h => h(App)
}).$mount('#app')
