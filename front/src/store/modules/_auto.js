const requireModule = require.context('.', true, /\.js$/)
const modules = {}

requireModule.keys().forEach(filename => {
  if (filename === './_auto.js' || filename.includes('useState')) return

  const moduleName = filename
    .replace(/(\.\/|\.js)/g, '')
    .replace(/^[^/]+\//, '')
    .replace(/^\w/, c => c.toUpperCase())

  modules[moduleName] =
    requireModule(filename).default || requireModule(filename)

  modules[moduleName].namespaced = true
})

export default modules
