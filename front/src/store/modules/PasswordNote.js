import { getAxios, postAxios, putAxios, deleteAxios } from '@/api/request'

const state = {
  all: {
    data: [],
    meta: {}
  },
  detail: {},
  progress: false,
  error: null
}
const mutations = {
  start: state => {
    state.progress = true
    state.all.data = []
    state.all.meta = {}
  },
  success: (state, { data, meta = {} }) => {
    state.progress = false
    state.all.data = data
    state.all.meta = meta
  },
  successLoad: (state, { data, meta }) => {
    state.all.data.push(...data)
    state.progress = false
    state.all.meta = meta
  },
  filter: (state, payload) => {
    state.filter = payload
  },
  filterField: (state, { field, value }) => {
    state.filter[field] = value
  },
  successDetail: (state, payload) => {
    state.detail = payload
  },
  fail: (state, payload) => {
    state.progress = true
    state.all.data = []
    state.error = payload
  }
}
const getters = {
  all: state => state.all,
  filter: state => state.filter,
  detail: state => state.detail,
  progress: state => state.progress,
  error: state => state.error
}

const actions = {
  async load({ commit, state, dispatch }) {
    return new Promise(() => {
      dispatch('getList').then(() => {
        for (let i = 2; i <= state.all.meta.last_page; ++i) {
          getAxios('password-note?page=' + i, state.filter)
            .then(response => {
              commit('successLoad', {
                data: response.data.data,
                meta: response.data.meta
              })
            })
            .catch(error => {
              console.warn(error)
            })
        }
      })
    })
  },
  async getList({ commit, state }) {
    commit('start')

    getAxios('password-note', state.filter)
      .then(response => {
        commit('success', {
          data: response.data.data,
          meta: response.data.meta
        })
      })
      .catch(error => {
        console.warn(error)
      })
  },
  async getItem({ commit }, id) {
    commit('start')
    return new Promise((resolve, reject) => {
      getAxios('password-note/' + id)
        .then(response => {
          commit('successDetail', response.data.data)
          resolve(response.data)
        })
        .catch(error => {
          console.warn(error)
          reject(error.response.data.message)
        })
    })
  },
  create({ commit }, data) {
    return new Promise((resolve, reject) => {
      postAxios('password-note', data)
        .then(response => {
          commit('successDetail', {
            data: response.data.result
          })
          resolve(response.data)
        })
        .catch(error => {
          console.warn(error)
          reject(error.response.data.message)
        })
    })
  },
  update({ commit }, data) {
    return new Promise((resolve, reject) => {
      putAxios('password-note/' + data.id, data)
        .then(response => {
          commit('successDetail', {
            data: response.data.result
          })
          resolve(response.data)
        })
        .catch(error => {
          console.warn(error)
          reject(error.response.data.message)
        })
    })
  },
  delete({ dispatch }, id) {
    return new Promise((resolve, reject) => {
      deleteAxios(`password-note/${id}`)
        .then(async response => {
          await dispatch('getList')
          resolve(response.data)
        })
        .catch(error => {
          console.warn(error)
          reject(error.response.data.message)
        })
    })
  }
}

export default { state, mutations, actions, getters }
