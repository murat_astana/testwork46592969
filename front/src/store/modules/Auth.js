import { postAxios } from '@/api/request'

const state = {
  validationErrors: null,
  token: false,
  user: {},
  isLoggedIn: !!window.localStorage.getItem('accessToken')
}

const mutations = {
  start: state => {
    state.validationErrors = null
  },
  success: (state, { token, data }) => {
    state.isLoggedIn = true
    state.validationErrors = null
    state.user = data
    window.localStorage.setItem('accessToken', token)
  },
  fail: (state, payload) => {
    state.isLoggedIn = false
    state.validationErrors = payload
  },
  logout: state => {
    window.localStorage.removeItem('accessToken')
    state.isLoggedIn = false
    state.validationErrors = null
    state.ncl = false
  }
}

const actions = {
  login({ commit }, data) {
    commit('start')

    return new Promise((resolve, reject) => {
      postAxios('/login', data)
        .then(async response => {
          const { token, data } = response.data

          await commit('success', { token, data })

          resolve(data)
        })
        .catch(error => {
          reject(error.response.data.message)
        })
    })
  },
  async logout({ commit }) {
    return new Promise(resolve => {
      commit('logout')
      resolve()
    })
  },
  async getUser({ commit }) {
    commit('start')
  }
}

const getters = {
  token: state => state.token,
  isLoggedIn: state => state.isLoggedIn,
  ncl: state => state.ncl,
  validationErrors: state => state.validationErrors,
  user: state => state.user
}

export default { state, mutations, actions, getters }
