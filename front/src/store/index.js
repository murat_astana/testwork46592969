import Vue from 'vue'
import Vuex from 'vuex'

import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex)

import modules from './modules/_auto'

const store = new Vuex.Store({
  modules: modules,
  plugins: [createPersistedState()]
})

export default store
