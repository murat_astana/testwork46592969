import Vue from 'vue'

import DefaultLayout from '@/layouts/Default'
import AuthLayout from '@/layouts/Auth'

import CreateForm from '@/components/form/CreateForm'
import EditForm from '@/components/form/EditForm'
import ShowForm from '@/components/form/ShowForm'
import EmailInput from '@/components/inputs/EmailInput'
import StringInput from '@/components/inputs/StringInput'
import TextInput from '@/components/inputs/TextInput'
import AddButton from '@/components/buttons/AddButton'
import ShowString from '@/components/show_input/ShowString'
import DefTable from '@/components/tables/DefTable'

// Layout
Vue.component('default-layout', DefaultLayout)
Vue.component('auth-layout', AuthLayout)

// Components

Vue.component('CreateForm', CreateForm)
Vue.component('EditForm', EditForm)
Vue.component('ShowForm', ShowForm)
Vue.component('EmailInput', EmailInput)
Vue.component('StringInput', StringInput)
Vue.component('TextInput', TextInput)
Vue.component('AddButton', AddButton)
Vue.component('ShowString', ShowString)
Vue.component('DefTable', DefTable)
