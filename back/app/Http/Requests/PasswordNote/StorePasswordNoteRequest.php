<?php

namespace App\Http\Requests\PasswordNote;

use Illuminate\Foundation\Http\FormRequest;

class StorePasswordNoteRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'url' => 'required|string|max:255',
            'login' => 'required|string|max:255',
            'password' => 'required|string|max:255',
            'note' => 'nullable|string|max:65535'
        ];
    }
}
