<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    function login (LoginRequest $request){
        if (!Auth::attempt(['email' => $request->input('email'), 'password' => $request->input('password')]) )
            return $this->errorResponse('Wrong credentials', 400);

        $user = Auth::user();

        return [
            'data' => $user,
            'token' =>  $user->createToken('login')->plainTextToken
        ];
    }

    function logout(Request $request){
        $request->user()->tokens()->delete();

        return true;
    }

    function getData(Request $request){
        return $request->user();
    }
}
