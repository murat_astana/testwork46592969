<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\PasswordNote\StorePasswordNoteRequest;
use App\Http\Resources\PasswordNoteResource;
use App\PasswordNote;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PasswordNoteController extends Controller
{

    public function index()
    {
        return PasswordNoteResource::collection(
                PasswordNote::latest()->paginate(24)
            );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Foundation\Http\FormRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StorePasswordNoteRequest $request)
    {
        DB::beginTransaction();
        try {
            $password_note = PasswordNote::create($request->validated());
            DB::commit();

            return $this->successResponse(new PasswordNoteResource($password_note), 'Успешно создано');
        } catch (\Exception $e) {
            DB::rollback();

            return $this->errorResponse($e->getMessage(), 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  PasswordNote  $password_note
     * @return PasswordNoteResource
     */
    public function show(PasswordNote $password_note)
    {
        return new PasswordNoteResource($password_note);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Foundation\Http\FormRequest  $request
     * @param  PasswordNote  $password_note
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(StorePasswordNoteRequest $request, PasswordNote $password_note)
    {
        DB::beginTransaction();
        try {
            $password_note->update($request->validated());
            DB::commit();

            return $this->successResponse(new PasswordNoteResource($password_note), 'Успешно изменено');
        } catch (\Exception $e) {
            DB::rollback();

            return $this->errorResponse($e->getMessage(), 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  PasswordNote  $password_note
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(PasswordNote $password_note)
    {
        DB::beginTransaction();
        try {
            $password_note->delete();
            DB::commit();

            return $this->successResponse(true, 'Успешно удалено');
        } catch (\Exception $e) {
            DB::rollback();

            return $this->errorResponse($e->getMessage(), 400);
        }
    }
}
