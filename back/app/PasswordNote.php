<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PasswordNote extends Model
{
    protected $table = 'password_notes';
    protected $fillable = ['name', 'url', 'login', 'password', 'note'];
}
