<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\PasswordNote;
use Faker\Generator as Faker;

$factory->define(PasswordNote::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'url' => $faker->url,
        'login' => $faker->userName,
        'password' => $faker->password,
        'note' => $faker->text
    ];
});
